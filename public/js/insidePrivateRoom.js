function init() {
    var user_email = '';
    var user_uid = "";
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('menu4account');
        // Check user login
        if (user) {
            user_uid = user.uid;
            user_email = user.email;
            var add_flag = true;
            var email2uidRef = firebase.database().ref("loggedUsers/");
            var counterNum = 0;
            email2uidRef.once('value')
                .then(function(snapshot){
                    snapshot.forEach(function (childSnapshot){
                        var childData = childSnapshot.val();
                        if (childData.email != user.email){
                            counterNum++;
                        }
                        else if (childData.email == user.email){
                            add_flag = false;
                        }
                    })
                    
                    if (add_flag){  
                        var email2uid = firebase.database().ref("loggedUsers/" + counterNum);
                        email2uid.update({
                            email: user.email,
                            uid: user.uid
                        });
                    }
            })
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutBtn = document.getElementById('logout-btn');
            var privateRoomBtn = document.getElementById('privateRoom');
            var publicRoomBtn = document.getElementById('publicRoom');
            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    // Sign-out successful.
                    window.location.assign("index.html");
                    alert("logout success");

                }).catch(function (error) {
                    // An error happened.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("logout error");
                });
            })
            privateRoomBtn.addEventListener('click', function () {
                window.location.assign("privateRoom.html");
            })

            publicRoomBtn.addEventListener('click', function () {
                window.location.assign("publicRoom.html");
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    
        var curRoom_name = "";
        var curRoomRef = firebase.database().ref("curRoom");
        var post_btn = document.getElementById('post_btn');
        var post_txt = document.getElementById('comment');

        var addFriend_btn = document.getElementById("addFriend_btn");
        var loggedUsersRef = firebase.database().ref("loggedUsers");

        addFriend_btn.addEventListener("click", function(){
            var friendEmail_txt = document.getElementById("friendEmail").value;
            var match = 0;
            var matched_user_uid = "";
            if (friendEmail_txt != ""){
                loggedUsersRef.once("value")
                    .then(function(snapshot){
                        snapshot.forEach(function(childSnapshot){
                            console.log("friendEmail_txt: ");
                            console.log(friendEmail_txt);
                            if (friendEmail_txt == childSnapshot.val().email){
                                match = 1;
                                matched_user_uid = childSnapshot.val().uid;
                            }
                            
                        });
                        console.log(match);
                        if (match){
                            curRoomRef.once('value', function(snapshot){
                                curRoom_name = snapshot.val().room_name;
                                var addSomeoneRef = firebase.database().ref("AllPrivateRooms/" + curRoom_name + "/accessedUsers/" + matched_user_uid);
                                addSomeoneRef.set(true);
                                console.log("users/" + matched_user_uid + "/roomList/" + curRoom_name);
                                var userHaveRoomRef = firebase.database().ref("users/" + matched_user_uid + "/roomList/" + curRoom_name);
                                let date = new Date();
                                userHaveRoomRef.set(date.getTime());
                            })
                        }else{
                            alert("The user doesn't exist !");
                        }
                        document.getElementById("friendEmail").value = "";
                    });
                
            }
        })


        post_btn.addEventListener('click', function () {
            if (post_txt.value != "") {
                curRoomRef.once('value', function(snapshot){
                    curRoom_name = snapshot.val().room_name;
                    var comListRef = firebase.database().ref("AllPrivateRooms/" + curRoom_name + "/messages");

                    var user = firebase.auth().currentUser;
                    if (user){
                        comListRef.push({
                            email: user.email,
                            comment: post_txt.value                   
                        })
                        post_txt.value = "";
                    } else {
                        alert("Not log in");
                    }
                })
            }
        });

        
        // The html code for post
        var str_before_username = "<div style='background-color:pink' class='my-2'><div class='pt-3'><p><strong class='d-block text-gray-dark'>";
        var str_after_content = "</p></div></div>\n";
        curRoomRef.once('value').then(function(snapshot){
            document.getElementById('myHeader').innerHTML = "You are in the Private Room: " + snapshot.val().room_name;
            curRoom_name = snapshot.val().room_name;
            console.log("curRoom_name");
            console.log(curRoom_name);

            var postsRef = firebase.database().ref("AllPrivateRooms/" + curRoom_name + "/messages");
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;
            postsRef.once('value').then(function (snapshot) {

                /// 整理好 total_post ， 之後一併送上給 postRef 給呈現在 html 上
                snapshot.forEach(function(childSnapshot){
                    total_post.push(str_before_username + childSnapshot.val().email + "</strong>" + "<xmp>" + childSnapshot.val().comment + "</xmp>" + str_after_content);
                    first_count++;
                })

                document.getElementById('post_list').innerHTML = total_post.join('');
                /// Add listener to update new post
                postsRef.on('child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "<xmp>" + childData.comment + "</xmp>" + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                        notifyMe();
                    }
                });
            }).catch(e => console.log(e.message));        
        })

    });
};

function notifyMe() {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
  
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      var notification = new Notification("There is a new message!");
    }
  
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          var notification = new Notification("There is a new message!");
        }
      });
    }
}

window.onload = function () {
    init();
};