function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('menu4account');
        // Check user login
        if (user) {
            user_email = user.email;
            var add_flag = true;
            var email2uidRef = firebase.database().ref("loggedUsers/");
            var counterNum = 0;
            email2uidRef.once('value')
                .then(function(snapshot){
                    //var counterNum = snapshot.numChildren(); 
                    snapshot.forEach(function (childSnapshot){
                        var childData = childSnapshot.val();
                        if (childData.email != user.email){
                            counterNum++;
                        }
                        else if (childData.email == user.email){
                            add_flag = false;
                        }
                    })
                    
                    console.log(counterNum);
                    if (add_flag){  
                        var email2uid = firebase.database().ref("loggedUsers/" + counterNum);
                        email2uid.update({
                            email: user.email,
                            uid: user.uid
                        });
                    }
            })
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutBtn = document.getElementById('logout-btn');
            var privateRoomBtn = document.getElementById('privateRoom');
            var publicRoomBtn = document.getElementById('publicRoom');
            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    // Sign-out successful.
                    window.location.assign("index.html");
                    alert("logout success");

                }).catch(function (error) {
                    // An error happened.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("logout error");
                });
            })
            privateRoomBtn.addEventListener('click', function () {
                window.location.assign("privateRoom.html");
            })

            publicRoomBtn.addEventListener('click', function () {
                window.location.assign("publicRoom.html");
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var comList = firebase.database().ref("com_list");
            var user = firebase.auth().currentUser;
            if (user){
                comList.push({
                    email: user.email,
                    comment: post_txt.value                   
                })
                post_txt.value = "";
            } else {
                alert("Not log in");
            }
            
        }
    }); 
};

function notifyMe() {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
      alert("This browser does not support desktop notification");
    }
  
    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      var notification = new Notification("There is a new message!");
    }
  
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          var notification = new Notification("There is a new message!");
        }
      });
    }
}

window.onload = function () {
    // The html code for post
    var str_before_username = "<div style='background-color:pink' class='my-2'><div class='pt-3'><p><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {

            /// Join all post in list to html in once
            
            for (let [key, value] of Object.entries(snapshot.val())){
                total_post.push(str_before_username + value.email + "</strong>" + "<xmp>" + value.comment + "</xmp>" + str_after_content);
                first_count++;
            }
            var list = document.getElementById('post_list').innerHTML = total_post.join('');
            /// Add listener to update new post
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + "<xmp>" + childData.comment + "</xmp>"+ str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    notifyMe();
                }
            });
        })
        .catch(e => console.log(e.message));

    init();
};