function init() {
    var user_email = '';
    /* 當使用者出在有無登入時的介面狀態改變 */
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('menu4account');

        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logoutBtn = document.getElementById('logout-btn');
            var privateRoomBtn = document.getElementById('privateRoom');
            var publicRoomBtn = document.getElementById('publicRoom');
            logoutBtn.addEventListener('click', function () {
                firebase.auth().signOut()
                .then(function () {
                    window.location.assign("index.html");
                    alert("logout success");

                }).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("logout error");
                });
            })
            /// TODO : 增加 eventListener 看看使用者有沒有要進 private room ，另外也要處理使用者點選 public room 時的應對方式!
            privateRoomBtn.addEventListener('click', function () {
                window.location.assign("privateRoom.html");
            })

            publicRoomBtn.addEventListener('click', function () {
                window.location.assign("publicRoom.html");
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('room_list').innerHTML = "";
        }
        waitList();
    });

    var post_btn = document.getElementById('post_btn');
    var room_name = document.getElementById('roomName');

    post_btn.addEventListener('click', function () {
        if (room_name.value != "") { 
            /* 當我創建一個 private room 之後，我要 
            1. 把該 room 的名稱存到該使用者可使用的 roomList 中
            2. 把該 room 的名稱存到 roomList 中，room 的名稱底下有 2 個 node，分別是可以使用 room 的人和該 room 中的 message
            */
            var user = firebase.auth().currentUser;
            if (user){
                var roomList_of_users = firebase.database().ref("users/" + user.uid + "/roomList/" + room_name.value);
                var all_private_room_list = firebase.database().ref("AllPrivateRooms/" + room_name.value  + "/accessedUsers/" + user.uid);
                var all_p_room_list = firebase.database().ref("AllPrivateRooms/" + room_name.value);
                
                let date = new Date();
                roomList_of_users.set(date.getTime());
                all_private_room_list.set(true);
                all_p_room_list.update({
                    room_name: room_name.value 
                })
                room_name.value = "";
            } else {
                alert("Not log in");
            }
            
        }
    });
    
};

function waitList(){
    // The html code for post
    var user = firebase.auth().currentUser;
    var str_before_username = "<option class='w3-btn w3-block w3-red' value="
    var middle = "><div class='pt-3'><p>";
    var str_after_content = "</p></div></option>";
    var roomRef = firebase.database().ref("users/" + user.uid + "/roomList");
    // List for store posts html
    var total_room = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    roomRef.once('value')
        .then(function (snapshot) {
            /// Join all post in list to html in once
            snapshot.forEach(function(childSnapshot){
                total_room.push(str_before_username + childSnapshot.key + middle + childSnapshot.key + str_after_content);
                first_count++;
            })

            document.getElementById('room_list').innerHTML = total_room.join('');
            /// Add listener to update new post
            roomRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.key;
                    total_room[total_room.length] = str_before_username + childData + middle + childData + str_after_content;
                    document.getElementById('room_list').innerHTML = total_room.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

function callDiffRoom(){
    var roomName = document.getElementById("room_list").value;
    var curRoomRef = firebase.database().ref("curRoom");
    curRoomRef.update({
        room_name: roomName
    })
    window.location.assign("insidePrivateRoom.html"); 
}

window.onload = function () {
    init();
};