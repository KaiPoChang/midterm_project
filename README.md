# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : midterm
* Key functions
    1. 帳號及密碼的創建與登入
    2. 使用現有的 google 帳號登入
    4. 私有聊天室
    5. 私有聊天室的朋友邀請功能
    6. 熱情打招呼的紅色方框
    7. 隨時切換公眾聊天室與私有聊天室
    8. 隨時查看當前使用者帳號的功能
    9. 隨時登出功能
    
* Other functions
    1. 公眾聊天室

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midchatroom.web.app/

# Components Description : 
1. Basic components : 
(1) Membership Mechanism
可以自己辦帳號以及設定密碼來創立帳號，並且登入使用聊天室。
(2) Firebase Page
以 firebase 為後端服務平台部屬聊天室，並且確認執行無誤。
(3) Database
藉由 firebase 資料庫的讀寫，儲存當前使用者、可使用的房間資訊、所有使用者名單、以及使用者存取房間的規範。
(4) RWD
利用 boostrap 輔助聊天室開發，以達到在不同的網頁框大小之下，介面的完整呈現與完整運行。
(5) Topic Key Function
實作私有聊天室，利用資料庫的讀寫限制使用者可存取房間的房間清單，當只有使用者被該房間創立的人邀請加入之後，才會擁有該房間的存取權，並擁有讀寫的權利。

2. Advanced components :
(1) Third-Party Sign In
可使用使用者現有的 google 帳號當作聊天室的帳號登入聊天室。
(2) Chrome Notification
當私有聊天室或公眾聊天室有新的使用者發出訊息時，電腦螢幕視窗右下角會顯示出 "There is a new messages" 的提醒訊息出現。
(3) CSS animation
當使用者一開始進入首頁，會有　＂Welcome!" 的紅色方框自己移動和使用者打招呼，歡迎使用者的到來。
(4) Security Report
當使用者在文字框中輸入訊息時，若是輸入 html code， 因為使用了 <xmp> 這個 tag ，所以 html code 也能和普通中英文一樣正常顯示在螢幕上。
# Other Functions Description : 
1. Public room 
實作出公眾聊天室，讓所有使用者擁有一個可以公眾交流的聊天平台。
